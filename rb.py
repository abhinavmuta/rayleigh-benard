from time import time
import numpy as np
from scipy.linalg import inv, eig
import matplotlib.pyplot as plt
from chebdiff import chebdiff


def reyleigh_benard(N, k, Ra, Pr, bc1, bc2):
    order = 6
    L = 0.5
    x, DM = chebdiff(N, order)
    x = x*L

    D2 = DM[1]/L**2
    D4 = DM[3]/L**4

    I = np.eye(N)
    k2 = k*k*I

    Ze = np.zeros((N, N))

    tmp = D2 - k2
    A = tmp/Pr
    B = tmp@tmp
    B = -(1/Pr+1)*B

    C = tmp@tmp@tmp + k2*Ra

    # LHS = np.vstack([np.hstack([-C, -B]), np.hstack([Ze, I])])
    # RHS = np.vstack([np.hstack([Ze, A]), np.hstack([I, Ze])])
    LHS = np.vstack([np.hstack([-C, Ze]), np.hstack([Ze, I])])
    RHS = np.vstack([np.hstack([B, A]), np.hstack([I, Ze])])

    wid = np.arange(N)
    zid = N + np.arange(N)

    C1 = np.zeros((order, 2*N))
    if (bc1 == 'free'):
        C1[0, wid] = I[wid[0], :]
        C1[1, wid] = D2[wid[0], :]
        C1[2, wid] = D4[wid[0], :]

    C1[3:, zid] = C1[:3, wid]
    r1id = np.array([wid[0], wid[1], wid[2], zid[0], zid[1], zid[2]])

    C2 = np.zeros((order, 2*N))
    if (bc2 == 'free'):
        C2[0, wid] = I[wid[-1], :]
        C2[1, wid] = D2[wid[-1], :]
        C2[2, wid] = D4[wid[-1], :]

    C2[3:, zid] = C2[:3, wid]
    r2id = np.array([wid[-1], wid[-2], wid[-3], zid[-1], zid[-2], zid[-3]])

    C = np.vstack([C1, C2])

    rid = np.concatenate([r1id, r2id])
    kid = np.setdiff1d(np.arange(2*N), rid)

    G = -inv(C[:, rid])@C[:, kid]

    LHS_k = LHS[np.ix_(kid, kid)] + LHS[np.ix_(kid, rid)]@G
    RHS_k = RHS[np.ix_(kid, kid)] + RHS[np.ix_(kid, rid)]@G

    print("computing eigenvalues")
    start = time()
    S, W_k = eig(LHS_k, RHS_k)
    print('elapsed time is ', time() - start, 'sec')

    ret = np.where((np.abs(S) < 100))[0]
    S = S[ret]
    W_k = W_k[:, ret]

    W = np.zeros((2*N, len(S)), dtype=np.complex)
    W[kid, :] = W_k
    W[rid, :] = G@W_k

    W = W[wid, :]

    return x, S, W


if __name__ == '__main__':
    N = 80
    k = np.pi/np.sqrt(2)
    Ra = 657
    Pr = 0.1
    x, S, W = reyleigh_benard(N, k, Ra, Pr, 'free', 'free')

    plt.figure()
    plt.scatter(np.real(S), np.imag(S))
    plt.xlabel('Re(S)')
    plt.ylabel('Im(S)')
    plt.title('Eigenvalues, S')
    plt.show()

    plt.subplot(1, 2, 1)
    aa = np.argmin(np.abs(np.imag(S)))
    plt.plot(x, np.real(W[:,aa]),'b', label='Wreal')
    plt.plot(x, np.imag(W[:,aa]),'r', label='Wimag')
    # plt.legend(loc='best')
    plt.xlabel('z')
    plt.ylabel('W')
    plt.title('Eigenfunction for ' + str(np.real(S[aa])) + ' '
              + str(np.imag(S[aa])) + 'j')
    # plt.subplot(1, 2, 2)
    # plt.plot(x, np.real(T[:,aa]),'b', label='Treal')
    # plt.plot(x, np.imag(T[:,aa]),'r', label='Timag')
    # plt.legend(loc='best')
    # plt.xlabel('z')
    # plt.ylabel('T')
    # plt.title('Eigenfunction for ' + str(np.real(S[aa])) + ' '
              # + str(np.imag(S[aa])) + 'j')
    plt.show()

