from time import time
import numpy as np
from scipy.linalg import inv, eig
import matplotlib
import matplotlib.pyplot as plt
from chebdiff import chebdiff


def rayleigh_benard(N, k, Ra, Pr, bc1, bc2):
    order = 4
    L = 0.5
    x, DM = chebdiff(N, order)
    x = x*L

    D1 = DM[0]/L**1
    D2 = DM[1]/L**2

    k2 = k*k

    I = np.eye(N)
    Ze = np.zeros((N, N))

    tmp = D2 - k2*I
    A = tmp
    B = tmp@tmp
    C = tmp/Pr

    LHS = np.vstack([np.hstack([I, A]), np.hstack([B, -k2*Ra*I])])
    RHS = np.vstack([np.hstack([Ze, I]), np.hstack([C, Ze])])

    wid = np.arange(N)
    tid = N + np.arange(N)

    I2 = np.eye(2*N)
    C1 = np.zeros((3, 2*N))
    if (bc1 == 'free'):
        C1[0, wid] = I[wid[0], :]
        C1[1, wid] = D2[wid[0], :]
        C1[2, tid] = I2[tid[0], tid[0]:]

    if (bc1 == 'rigid'):
        C1[0, wid] = I[wid[0], :]
        C1[1, wid] = D1[wid[0], :]
        C1[2, tid] = I2[tid[0], tid[0]:]

    r1id = np.array([wid[0], wid[1], tid[0]])

    C2 = np.zeros((3, 2*N))
    if (bc2 == 'free'):
        C2[0, wid] = I[wid[-1], :]
        C2[1, wid] = D2[wid[-1], :]
        C2[2, tid] = I2[tid[-1], tid[0]:]
    if (bc2 == 'rigid'):
        C2[0, wid] = I[wid[-1], :]
        C2[1, wid] = D1[wid[-1], :]
        C2[2, tid] = I2[tid[-1], tid[0]:]

    r2id = np.array([wid[-1], wid[-2], tid[-1]])

    C = np.vstack([C1, C2])

    rid = np.concatenate([r1id, r2id])
    kid = np.setdiff1d(np.arange(2*N), rid)

    G = -inv(C[:, rid])@C[:, kid]

    LHS_k = LHS[np.ix_(kid, kid)] + LHS[np.ix_(kid, rid)]@G
    RHS_k = RHS[np.ix_(kid, kid)] + RHS[np.ix_(kid, rid)]@G

    print("computing eigenvalues")
    start = time()
    S, P_k = eig(LHS_k, RHS_k)
    print('elapsed time is ', time() - start, 'sec')

    ret = np.where((np.abs(S) < 200))[0]
    S = S[ret]
    P_k = P_k[:, ret]

    P = np.zeros((2*N, len(S)), dtype=np.complex)
    P[kid, :] = P_k
    P[rid, :] = G@P_k

    W = P[wid, :]
    T = P[tid, :]
    return x, S, W, T


def main(N, Pr):
    Sg = []; Wg = []; Tg = []
    bcs = [('free', 'free'), ('rigid', 'free'), ('free', 'rigid'), ('rigid',
                                                                     'rigid')]
    Ra = [657.5, 1100.65, 1100.65, 1707.67, 4000]
    k = [2.221, 2.682, 2.682, 3.117]
    i = 0
    for bc1, bc2 in bcs:
        x, S, W, T = rayleigh_benard(N, k[i], Ra[i], Pr, bc1, bc2)
        Sg += [S]
        Wg += [W]
        Tg += [T]
        x = x[::-1]
        i += 1

    f = plt.figure(figsize=(8,11))
    for i in range(len(bcs)):
        txt = bcs[i][0] + ' ' + bcs[i][1]
        f.add_subplot(len(bcs), 1, i+1)
        plt.scatter(np.real(Sg[i]), np.imag(Sg[i]))
        plt.ylabel('Im(S)')
        plt.title('Eigenvalues, S, {}'.format(txt))
        plt.tick_params(direction='in')
    plt.xlabel('Re(S)')
    plt.show()
    plt.savefig('ev.pdf')

    tmp = 1
    f = plt.figure(figsize=(8,11))
    for i in range(len(bcs)):
        txt = bcs[i][0] + ' ' + bcs[i][1]
        f.add_subplot(len(bcs), 2, tmp)
        aa = np.argmin(np.abs(np.imag(Sg[i])))
        plt.plot(x, np.real(Wg[i][:,aa]),'b', label='Wreal')
        plt.plot(x, np.imag(Wg[i][:,aa]),'r', label='Wimag')
        plt.legend(loc='best')
        plt.ylabel('W')
        plt.xlim(-0.5, 0.5)
        plt.title(r'$\hat W$ ' + '{:.2f} + {:.2}j, {}'.format(np.real(Sg[i][aa]),
                                                     np.imag(Sg[i][aa]), txt))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        f.add_subplot(len(bcs), 2, tmp+1)
        plt.plot(x, np.real(Tg[i][:,aa]),'b', label='Treal')
        plt.plot(x, np.imag(Tg[i][:,aa]),'r', label='Timag')
        plt.legend(loc='best')
        plt.ylabel('T')
        plt.title(r'$\hat T$ ' + '{:.2f} + {:.2}j, {}'.format(np.real(Sg[i][aa]),
                                                     np.imag(Sg[i][aa]), txt))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        tmp += 2
    plt.xlabel('z')
    plt.savefig('ef.pdf')


if __name__ == '__main__':
    N = 120
    Pr = 0.1
    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 8}

    matplotlib.rc('font', **font)
    matplotlib.rc('axes', titlesize=10)
    main(N, Pr)
