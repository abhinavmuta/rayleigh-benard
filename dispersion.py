import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def cal(nx, b):
    k = np.linspace(0, 3, nx, dtype=np.complex)
    ze = 1/np.tanh(k*0.5)
    b1 = 1-b
    b2 = 1-2*b
    zep = (ze + 1)
    zem = (ze - 1)

    A = 2*k*k*zep
    B = -2*k*zem*b1*np.exp(-k) - 2*k*(zep*(k*(1+b) - b1) + 2*b2)
    C = 2*b1*(b*k*zem + 2*b2)*np.exp(-k) - 2*(b1-k)*(b*k*(zep) + 2*b2)
    return k[1:], A[1:], B[1:], C[1:]


def calb(nx, k):
    b = np.linspace(0, 1.5, nx, dtype=np.complex)
    ze = 1/np.tanh(k*0.5)
    b1 = 1-b
    b2 = 1-2*b
    zep = (ze + 1)
    zem = (ze - 1)

    A = 2*k*k*zep
    B = -2*k*zem*b1*np.exp(-k) - 2*k*(zep*(k*(1+b) - b1) + 2*b2)
    C = 2*b1*(b*k*zem + 2*b2)*np.exp(-k) - 2*(b1-k)*(b*k*(zep) + 2*b2)
    A = np.array([A]*len(B))
    return b[1:], A[1:], B[1:], C[1:]


def main():
    k, A, B, C = cal(1000, b=0.4)
    r1 = np.zeros_like(A)
    r2 = np.zeros_like(A)
    for i in range(len(A)):
        r1[i], r2[i] = np.roots([A[i], B[i], C[i]])

    f = plt.figure(figsize=(8,11))
    f.add_subplot(2, 2, 1)
    plt.suptitle("Dispersion relation")
    plt.title("b = 0.4, phase velocity")
    plt.plot(np.real(k), np.real(r1))
    plt.plot(np.real(k), np.real(r2))
    plt.xlabel('k')
    plt.ylabel('Cr')
    f.add_subplot(2, 2, 2)
    plt.title("b = 0.4, growth rate")
    plt.plot(np.real(k), np.imag(r1))
    plt.plot(np.real(k), np.imag(r2))
    plt.xlabel('k')
    plt.ylabel('Ci')


    k, A, B, C = calb(1000, k=1)
    r1 = np.zeros_like(B)
    r2 = np.zeros_like(B)
    for i in range(len(A)):
        r1[i], r2[i] = np.roots([A[i], B[i], C[i]])

    f.add_subplot(2, 2, 3)
    plt.title("k = 1.0, phase velocity")
    plt.plot(np.real(k), np.real(r1))
    plt.plot(np.real(k), np.real(r2))
    plt.xlabel('b')
    plt.ylabel('Cr')
    f.add_subplot(2, 2, 4)
    plt.title("k = 1.0, growth rate")
    plt.plot(np.real(k), np.imag(r1))
    plt.plot(np.real(k), np.imag(r2))
    plt.xlabel('b')
    plt.ylabel('Ci')
    plt.savefig("dispersion.pdf")
    plt.show()


if __name__ == '__main__':
    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 8}

    matplotlib.rc('font', **font)
    matplotlib.rc('axes', titlesize=10)
    main()

