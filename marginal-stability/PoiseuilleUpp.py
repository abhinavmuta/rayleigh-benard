import numpy as np


def PoiseuilleUpp(y):
    return -2.0 * np.ones_like(y)

