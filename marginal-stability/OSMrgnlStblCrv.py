import numpy as np
from collections import namedtuple
from OSMrgnlStblAlph import OSMrgnlStblAlph
from OSMrgnlStblR import OSMrgnlStblR
import matplotlib.pyplot as plt


flagBF = 1

options = namedtuple('optionsMrgnl', 'maxiter ftolAbs xtolRel xRelxn xStep0\
                      verbosity')
optShoot = namedtuple('optionsMrgnl', 'maxiter ftolAbs xtolRel xRelxn xStep0\
                      NOrth verbosity')
options.maxiter = 40
options.ftolAbs = 1e-6
options.xtolRel = 1e-5
options.xRelxn = 0.2
options.xStep0 = 0.01
options.verbosity = 0

optShoot.maxiter = 100
optShoot.ftolAbs = 1e-10
optShoot.xtolRel = 1e-8
optShoot.xRelxn = 1.0
optShoot.xStep0 = 0.01
optShoot.NOrth = 50
optShoot.verbosity = 0



Npts = 10
Rs = np.linspace(11180, 5772, Npts)
alphs = np.zeros_like(Rs)
cs = np.zeros_like(Rs, dtype=np.complex)
alph0 = 1.09
c0 = 0.24 + 0.00011543j


for i in range(Npts):
    alphs[i], cs[i] = OSMrgnlStblAlph(flagBF, Rs[i], alph0, c0, options,
                                     optShoot)

    if alphs[i] is None:
        print("Marginalization error at i = {} for R = {}".format(i, Rs[i]))

        if i > 0:
            alphs[i] = alphs[i-1]
        else:
            alphs[i] = alph0
    print("i = {}, R = {}, alph = {}, c = {}".format(i, Rs[i], alphs[i],
                                                     cs[i]))
    alph0 = alphs[i]
    c0 = cs[i]

plt.figure()
plt.plot(Rs, alphs)

Npts = 10
alphs = np.linspace(1.085, 0.9835, Npts)
Rs = np.zeros_like(alphs)
cs = np.zeros_like(alphs, dtype=np.complex)
R0 = 6526
c0 = 0.265 + 0.00011543j


for i in range(Npts):
    Rs[i], cs[i] = OSMrgnlStblR(flagBF, alphs[i], R0, c0, options,
                                     optShoot)

    if Rs[i] is None:
        print("Marginalization error at {} for alph = {}".format(i, alphs[i]))
        if i > 0:
            Rs[i] = Rs[i-1]
        else:
            Rs[i] = R0
    print("i = {}, R = {}, alph = {}, c = {}".format(i, Rs[i], alphs[i],
                                                     cs[i]))
    R0 = Rs[i]
    c0 = cs[i]

plt.plot(Rs, alphs)
plt.show()
