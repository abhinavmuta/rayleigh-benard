import numpy as np
from OSshooting import OSshooting


def OSMrgnlStblR(flagBF, alph, R0, c0, opts, optShoot):
    R1 = R0
    c1 = OSshooting(flagBF, alph, R1, c0, optShoot)
    if c1 is None:
        if opts.verbosity > 0:
            print("Solution error at iter = -1 for R\
                            = {}".format(R1))
        return [], []

    R2 = R0 * (1 + opts.xStep0)
    c2 = OSshooting(flagBF, alph, R2, c0, optShoot)
    if c2 is None:
        if opts.verbosity > 0:
            print("Solution error at iter = 0 for R\
                            = {}".format(R2))
        return [], []

    if opts.verbosity > 2:
        print("iter = -1, R = {}, c = {}".format(R1, c1))
        print("iter = 0, R = {}, c = {}".format(R2, c2))

    for i in range(opts.maxiter):
        if abs(np.imag(c2)) < opts.ftolAbs:
            if opts.verbosity > 1:
                print("iter = {}, ci = {}; stop".format(i, np.imag(c2)))
            R = R2
            c = c2
            break

        R = (R2 - np.imag(c2) * (R1 - R2) / (np.imag(c1) - np.imag(c2))*
             opts.xRelxn)
        if abs(R - R2) / abs(R2) < opts.xtolRel:
            if opts.verbosity > 1:
                print("iter = {}, R = {}, diff R = {}, ci = {};\
                      breaking".format(i, R, abs(R2-R), np.imag(c2)))
            R = R2
            c = c2
            break
        c = OSshooting(flagBF, alph, R, c2, optShoot)
        if c is None:
            if opts.verbosity > 0:
                print("Solution error at iter = {}, for R\
                                = ".format(i, R))
            return [], []
        R1 = R2
        R2 = R
        c1 = c2
        c2 = c
        if opts.verbosity > 2:
            print("iter = {}, R = {}, c = {}".format(i, R, c2))

    if iter == opts.maxiter:
        if opts.verbosity > 0:
            print("Max iters reached")
        return [], []
    return R, c
