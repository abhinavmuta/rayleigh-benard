import numpy as np
from OSshooting import OSshooting


def OSMrgnlStblAlph(flagBF, R, alph0, c0, opts, optShoot):
    alph1 = alph0
    c1 = OSshooting(flagBF, alph1, R, c0, optShoot)
    if c1 is None:
        if opts.verbosity > 0:
            print("Solution error at iter = -1 for alph\
                            = {}".format(alph1))
        return [], []

    alph2 = alph0 * (1 + opts.xStep0)
    c2 = OSshooting(flagBF, alph2, R, c0, optShoot)
    if c2 is None:
        if opts.verbosity > 0:
            print("Solution error at iter = 0 for alph\
                            = {}".format(alph2))
        return [], []

    if opts.verbosity > 2:
        print("iter = -1, alph = {}, c = {}".format(alph1, c1))
        print("iter = 0, alph = {}, c = {}".format(alph2, c2))

    for i in range(opts.maxiter):
        if abs(np.imag(c2)) < opts.ftolAbs:
            if opts.verbosity > 1:
                print("iter = {}, ci = {}; stop".format(i, np.imag(c2)))

            alph = alph2
            c = c2
            break

        alph = (alph2 - np.imag(c2) * (alph1 - alph2) / (np.imag(c1)
                                                         - np.imag(c2))
                * opts.xRelxn)
        if abs(alph - alph2) / abs(alph2) < opts.xtolRel:
            if opts.verbosity > 1:
                print("iter = {}, alpha = {}, diff alpha = {}, ci = {};\
                      breaking".format(i, alph, abs(alph2-alph), np.imag(c2)))
            alph = alph2
            c = c2
            break
        c = OSshooting(flagBF, alph, R, c2, optShoot)
        if c is None:
            if opts.verbosity > 0:
                raise Exception("Solution error at iter = {}, for alph\
                                = ".format(i, alph))
            return [], []
        alph1 = alph2
        alph2 = alph
        c1 = c2
        c2 = c
        if opts.verbosity > 2:
            print("iter = {}, alph = {}, c = {}".format(i, alph, c2))

    if iter == opts.maxiter:
        if opts.verbosity > 0:
            print("Max iters reached")
        return [], []
    return alph, c
