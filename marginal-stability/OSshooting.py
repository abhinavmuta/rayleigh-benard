import numpy as np
from scipy.integrate import RK45, solve_ivp
from PoiseuilleU import PoiseuilleU
from PoiseuilleUpp import PoiseuilleUpp


def OSshooting(flagBF, alph, R, c0, opts):
    c1 = c0
    d1 = OSIntegrate(opts.NOrth, flagBF, alph, R, c1)
    c2 = c0 * (1 + opts.xStep0)
    d2 = OSIntegrate(opts.NOrth, flagBF, alph, R, c2)
    if opts.verbosity > 2:
        print("iter = -1, c = {}, d = {}".format(c1, d1))
        print("iter = 0, c = {}, d = {}".format(c1, d1))

    for i in range(opts.maxiter):
        if abs(d2) < opts.ftolAbs:
            if opts.verbosity > 1:
                print("iter = {}, d = {}; stop".format(i, d2))
            c = c2
            break
        c = c2 - d2*(c1 - c2)/(d1 - d2)*opts.xRelxn
        if abs(c - c2)/abs(c2) < opts.xtolRel:
            if opts.verbosity > 1:
                print("iter = {}, c = {}, diff c = {}; breaking".format(i, c,
                                                                        abs(c2-c)))
            break
        d = OSIntegrate(opts.NOrth, flagBF, alph, R, c)
        c1 = c2
        c2 = c
        d1 = d2
        d2 = d
        if opts.verbosity > 2:
            print("iter = {}, c = {}, d = {}".format(i, c, d))

    if i >= opts.maxiter:
        if opts.verbosity > 0:
            print("Max iters reached")
        return []
    return c


def OSIntegrate(NOrth, flagBF, alph, R, c):
    Phi10 = np.array([0, 0, 0, 1], dtype=np.complex)
    Phi20 = np.array([0, 0, 1, 0], dtype=np.complex)
    zAxisOrth = np.linspace(-1, 1, NOrth)
    yAxisOrth = zAxisOrth * np.exp(zAxisOrth**2 - 1)
    Psi10 = Phi10
    Psi20 = Phi20

    for iStp in range(NOrth-1):
        ySpan = yAxisOrth[iStp:iStp+2]
        Psi1 = OSIntegratePart(ySpan, Psi10, flagBF, alph, R, c)
        Psi2 = OSIntegratePart(ySpan, Psi20, flagBF, alph, R, c)

        A = np.vstack([Psi1, Psi2])
        p, _ = np.linalg.qr(A.T)

        Psi10 = p[:, 0]
        Psi20 = p[:, 1]

    dVal = Psi10[0]*Psi20[1] - Psi10[1]*Psi20[0]
    dVal = np.linalg.det(p[:2, :2])
    return dVal


def OSIntegratePart(ySpan, Phi0, flagBF, alph, R, c):
    if flagBF == 1:
        uFnc = PoiseuilleU
        uppFnc = PoiseuilleUpp
    r = solve_ivp(lambda t, y: OSop(t, y, uFnc, uppFnc, alph, R, c),
                  ySpan, Phi0, method=RK45)
    return r.y[:, -1]


def OSop(y, Phi, UFnc, UppFnc, alph, R, c):
    a = -1j * alph * R * (alph**2 * (UFnc(y) - c) + UppFnc(y)) - alph**4.0
    b = 1j * alph * R * (UFnc(y) - c) + 2 * alph**2.0
    PhiP = np.asarray([Phi[1] , Phi[2], Phi[3], a*Phi[0] + b*Phi[2]])
    return PhiP
